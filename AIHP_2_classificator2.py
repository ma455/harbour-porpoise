# %%
from sklearn.metrics import \
    classification_report, confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, \
    ConfusionMatrixDisplay
import joblib
from sklearn.ensemble import RandomForestClassifier
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler
import pandas as pd
import glob


def combine_excel_files_vertically(directory_path):
    # Step 1: Get a list of
    # all Excel files in the specified directory
    excel_files = glob.glob(f'{directory_path}/*.xlsx')

    # Step 2: Initialize an empty list to store DataFrames
    dfs = []

    # Step 3: Loop through each
    # Excel file and append its data to the list
    for file in excel_files:
        df = pd.read_excel(file)
        dfs.append(df)

    # Step 4: Use pandas.concat to combine
    # all DataFrames in the list vertically
    combined_df = pd.concat(dfs, axis=0, ignore_index=False)

    return combined_df


# Example usage:
directory_path = '/home/majd/Desktop/Arbeit/Project'
# Replace with the actual path
result_df = combine_excel_files_vertically(directory_path)


# %%
columns_names = ['NofClx', 'nActualClx', 'medianKHz', 'avEndF',
                 'nRisingIPIs', 'avSPL', 'avPkAt', 'avBWx8', 'TrDur_us',
                 'AvPRF',
                 'nICIrising', 'MinICI_us', 'midpointICI', 'MaxICI_us',
                 'ClkNofMinICI',
                 'ClkNofMaxICI', 'NofClstrs', 'avClstrNx8', 'avclF0',
                 'avclF1',
                 'avPkIPI', 'BeforeIPIratio', 'PreIPIratio',
                 'Post1IPIratio',
                 'Post2IPIratio', 'EndIPIratio']


# %%
def build_input_data(result_df, columns_names):
    X = result_df[columns_names]
    return X

# %%


def y_cleaning(y):
    y = result_df[['HP']]
    y = y.fillna(0)
    y = y.replace('?', 1)
    return y


# %%
X = build_input_data(result_df, columns_names)
y = y_cleaning(result_df[['HP']])

# %%


def train_test_split(X, y):
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X.values, y.values,
                                                        test_size=0.2, random_state=42)
    return X_train, X_test, y_train, y_test


# %%
X_train, X_test, y_train, y_test = train_test_split(X, y)

# %%
y_train_transform = y_train.ravel()
y_test_transform = y_test.ravel()

# %%
'''

from sklearn.preprocessing import MinMaxScaler

def normalize_data(X_train, X_test):
    scaler = MinMaxScaler()
    X_train_normalized = scaler.fit_transform(X_train)
    X_test_normalized = scaler.transform(X_test)
    return X_train_normalized, X_test_normalized


    
'''

# %%


def preprocss_data(X_train, X_test, y_train_transform, y_test_transform):
    scaler = StandardScaler()
    minimax = MinMaxScaler()
    robustscaler = RobustScaler()
    smote = SMOTE(random_state=42)
    X_train = minimax.fit_transform(X_train)
    X_test = minimax.transform(X_test)
    X_train = robustscaler.fit_transform(X_train)
    X_test = robustscaler.transform(X_test)

    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    X_train_resampled, y_train_resampled = smote.fit_resample(
        X_train, y_train_transform)
    X_test_resampled, y_test_resampled = smote.fit_resample(
        X_test, y_test_transform)

    return X_train_resampled, X_test_resampled, y_train_resampled, y_test_resampled


X_train_resampled, X_test_resampled, y_train_resampled, y_test_resampled = preprocss_data(X_train, X_test,
                                                                                          y_train_transform, y_test_transform)

# %%
# X_train_normalized, X_test_normalized = normalize_data(X_train, X_test)

# %%


def train_ForestClassifier(X_train, y_train):
    clf = RandomForestClassifier(n_estimators=800, random_state=42)
    clf.fit(X_train, y_train)
    return clf


# %%
print(X_train_resampled.shape)
print(y_train_resampled.shape)


# %%
randomforest = train_ForestClassifier(X_train_resampled, y_train_resampled)

# %%
joblib.dump(randomforest, 'randomforest_model.joblib')

# %%
predictions = randomforest.predict(X_test_resampled)

# %%
# Evaluate on the test set
accuracy = accuracy_score(y_test_resampled, predictions)
print("Accuracy:", accuracy)


cm = confusion_matrix(y_test_resampled, predictions,
                      labels=randomforest.classes_)
disp = ConfusionMatrixDisplay(cm, display_labels=randomforest.classes_)
disp.plot()
plt.show()
print("Classification Report:\n",
      classification_report(y_test_resampled, predictions))
print("Confusion Matrix:\n", cm)


# %%

loaded_model = joblib.load('randomforest_model.joblib')

# %%
predictions_new = loaded_model.predict(X_test_resampled)

# %%
predictions_new.shape

# %%

# Assuming predictions_new is a NumPy array
unique_values, counts = np.unique(predictions_new, return_counts=True)

# Display the counts for each unique value
for value, count in zip(unique_values, counts):
    print(f"Value {value}: Count {count}")
