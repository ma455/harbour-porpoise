import streamlit as st
import pandas as pd

# Load data
df = pd.read_excel('DE_PAL_6881_RD_230403_train_ok.xlsx')

# Streamlit app
st.title('Excel Data Analysis App')

# Display data
st.write('## Display Data')
st.dataframe(df)

# Basic statistics
st.write('## Basic Statistics')
st.write(df.describe())

# Data Visualization
st.write('## Data Visualization')
st.line_chart(df['NofClx'])
